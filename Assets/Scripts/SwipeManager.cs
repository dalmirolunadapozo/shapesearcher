﻿using UnityEngine;

public class SwipeManager : MonoBehaviour
{
    #region Instance
    private static SwipeManager instance;
    public static SwipeManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<SwipeManager>();
                if (instance == null)
                {
                    instance = new GameObject("Spawned SwipeInput", typeof(SwipeManager)).GetComponent<SwipeManager>();
                }
            }

            return instance;
        }
        set
        {
            instance = value;
        }
    }
    #endregion

    [Header("Tweaks")]
    [SerializeField] private float deadzone = 100.0f;
    [SerializeField] private float doubleTapDelta = 0.5f;
    [SerializeField] private float fingermove = 50.0f;

    [Header("Logic")]
    private bool tap, doubleTap, swipeLeft, swipeRight, swipeUp, swipeDown;
    private Vector2 startTouch, swipeDelta;
    private float lastTap;
    private float sqrDeadzone;

    #region Public Properties
    public bool Tap { get { return tap; } }
    public bool DoubleTap { get { return doubleTap; } }
    public Vector2 SwipeDelta { get { return swipeDelta; } }
    public bool SwipeLeft { get { return swipeLeft; } }
    public bool SwipeRight { get { return swipeRight; } }
    public bool SwipeUp { get { return swipeUp; } }
    public bool SwipeDown { get { return swipeDown; } }

    #endregion

    private void Start()
    {
        sqrDeadzone = deadzone * deadzone;
    }

    void Update()
    {
        tap = swipeDown = swipeUp = swipeRight = swipeLeft = false;

#if UNITY_EDITOR
        UpdateStandAlone();
#else
        UpdateMobile();
#endif



    }

    private void Reset()
    {
        startTouch = swipeDelta = Vector2.zero;
    }

    private void UpdateStandAlone()
    {
        if (Input.GetMouseButtonDown(0))
        {
            tap = true;
            startTouch = Input.mousePosition;
            doubleTap = Time.time - lastTap < doubleTapDelta;
            lastTap = Time.time;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            startTouch = swipeDelta = Vector2.zero;
        }

        swipeDelta = Vector2.zero;

        if (startTouch != Vector2.zero && Input.GetMouseButton(0))
        {
            swipeDelta = (Vector2)Input.mousePosition - startTouch;
        }

        if (swipeDelta.sqrMagnitude > sqrDeadzone)
        {
            if (swipeDelta.magnitude > fingermove)
            {
                float x = swipeDelta.x;
                float y = swipeDelta.y;

                if (Mathf.Abs(x) > Mathf.Abs(y))
                {
                    if (x < 0)
                    {
                        swipeLeft = true;
                    }
                    else
                    {
                        swipeRight = true;
                    }

                }
                else
                {
                    if (y < 0)
                    {
                        swipeDown = true;
                    }
                    else
                    {
                        swipeUp = true;
                    }
                }

                Reset();
            }
        }
    }
    private void UpdateMobile()
    {

        if (Input.touches.Length != 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                tap = true;
                startTouch = Input.mousePosition;
                doubleTap = Time.time - lastTap < doubleTapDelta;
                Debug.Log(Time.time - lastTap);
                lastTap = Time.time;
            }
        }
        else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
        {
            startTouch = swipeDelta = Vector2.zero;
        }

        swipeDelta = Vector2.zero;

        if (startTouch != Vector2.zero && Input.touches.Length != 0)
        {
            swipeDelta = Input.touches[0].position - startTouch;
        }

        if (swipeDelta.sqrMagnitude > sqrDeadzone)
        {
            if (swipeDelta.magnitude > fingermove)
            {
                float x = swipeDelta.x;
                float y = swipeDelta.y;

                if (Mathf.Abs(x) > Mathf.Abs(y))
                {
                    if (x < 0)
                    {
                        swipeLeft = true;
                        Debug.Log("Swipe left");
                    }
                    else
                    {
                        swipeRight = true;
                        Debug.Log("Swipe Right");
                    }

                }
                else
                {
                    if (y < 0)
                    {
                        swipeDown = true;
                        Debug.Log("Swipe Down");
                    }
                    else
                    {
                        swipeUp = true;
                        Debug.Log("Swipe Up");
                    }
                }

                Reset();
            }
        }
    }
}
