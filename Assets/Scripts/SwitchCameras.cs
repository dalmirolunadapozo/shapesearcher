﻿using UnityEngine;
using System.Collections;

public class SwitchCameras : MonoBehaviour
{
    [SerializeField] Camera CameraOn = null;
    [SerializeField] Camera CameraOff = null;

    public float time = 1.0f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("passed");
            StartCoroutine("HasPassed");
            //SwitchBeetwenCameras();
        }
    }

    private void SwitchBeetwenCameras()
    {
        if (CameraOn.gameObject.activeSelf == true)
        {
            CameraOn.gameObject.SetActive(false);
            CameraOff.gameObject.SetActive(true);
        }
        else
        {
            CameraOn.gameObject.SetActive(true);
            CameraOff.gameObject.SetActive(false);
        }
    }

    public IEnumerator HasPassed()
    {
        CameraMove.moveCamera = true;
        yield return new WaitForSeconds(time);
        CameraMove.moveCamera = false;
    }
}
