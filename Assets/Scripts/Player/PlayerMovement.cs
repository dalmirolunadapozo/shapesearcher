﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float speed = 5.0f;
    [SerializeField] private SwipeDetector swipe = null;

    private Rigidbody2D rigidBody;
    private Animator animator;
    public SpriteRenderer sprite = null;



    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        SwipeDetector.OnSwipe += SwipeDetector_OnSwipe;
    }


    private void Update()
    {
        animator.SetFloat("Speed", rigidBody.velocity.magnitude);
    }

    private void FixedUpdate()
    {

    }

    private void SwipeDetector_OnSwipe(SwipeData data)
    {
        if (data.Direction == SwipeDirection.Up)
        {
            transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
            rigidBody.velocity = new Vector2(0.0f, speed * Time.deltaTime);
        }
        else if (data.Direction == SwipeDirection.Down)
        {
            transform.rotation = Quaternion.Euler(0.0f, 0.0f, -180.0f);
            rigidBody.velocity = new Vector2(0.0f, -speed * Time.deltaTime);
        }
        else if (data.Direction == SwipeDirection.Left)
        {
            sprite.flipX = true;
            transform.rotation = Quaternion.Euler(0.0f, 0.0f, 90.0f);
            rigidBody.velocity = new Vector2(-speed * Time.deltaTime, 0.0f);
        }
        else if (data.Direction == SwipeDirection.Right)
        {
            sprite.flipX = false;
            transform.rotation = Quaternion.Euler(0.0f, 0.0f, -90.0f);
            rigidBody.velocity = new Vector2(speed * Time.deltaTime, 0.0f);
        }
    }


}