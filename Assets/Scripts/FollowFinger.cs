﻿using UnityEngine;

public class FollowFinger : MonoBehaviour
{

    private Vector3 touchPosition;
    [SerializeField] private float speed = 1.0f;
    private Rigidbody2D rigidBody;

    Vector2 position = new Vector2(0.0f, 0.0f);

    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
    }


    void Update()
    {
        if (Input.touchCount > 0)
        {

            Touch touch = Input.GetTouch(0);
            touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
            touchPosition.z = 0f;
            position = Vector2.Lerp(transform.position, touchPosition, speed);
        }
        
    }

    private void FixedUpdate()
    {
        rigidBody.MovePosition(position);
    }

}
