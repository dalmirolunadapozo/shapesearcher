﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public static bool moveCamera = false;

    public float speed = 0.10f;

    Vector3 newPosition;

    void Update()
    {
        newPosition = new Vector3(transform.position.x, transform.position.y + 9f, transform.position.z);
       
        
    }

    private void FixedUpdate()
    {
        if (moveCamera)
        {
            transform.position = Vector3.Lerp(transform.position, newPosition, speed * Time.deltaTime);
        }
    }

}
